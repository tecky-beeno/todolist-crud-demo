## setup npm project
yarn init

## install npm packages
```bash
yarn add knex pg express dotenv body-parser
yarn add -D typescript ts-node @types/express @types/body-parser
```

## setup tsconfig.json
```
{
    "compilerOptions": {
        "esModuleInterop": true
    }
}
```
This step is required to use `dotenv.config()` in next step

## setup knex
1. run `yarn knex init -x ts`
2. copy the settings from staging to developement
3. place username, password, and database in `.env`
4. load the `.env` file using `dotenv.config()`

## setup database tables
1. run `yarn knex migrate:make create-tables`
2. update the file under `migrations/yyyymmddhhmmss_create-tables.ts`, update the `up()` and `down()`

The `up()` write the code for the migration, e.g. create table with some columns;

the `down()` write the codes that cancel out the code in the `up()`, e.g. drop table

3. run the migration with `yarn knex migrate:up`

## setup express app
in server.ts
```typescript
import express from 'express'

let app = express()
```