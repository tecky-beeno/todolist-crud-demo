import express from 'express';
import bodyParser from 'body-parser';
import Knex from 'knex';
import multer from 'multer';
import fs from 'fs';
import path from 'path';

let upload = multer({ dest: 'uploads/' });

let knexfile = require('./knexfile');
let knex = Knex(knexfile.development);
let app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/task', upload.single('image'), async (req, res) => {
  console.log(req.method, req.url, {
    body: req.body,
    file: req.file,
  });

  try {
    await knex
      .insert({
        content: req.body.content,
        done: false,
        // image: req.file ? req.file.filename : null
        image: req.file?.filename,
      })
      .into('task');

    res.redirect('/');
  } catch (error) {
    res.status(500).json({
      message: 'failed to insert new task',
      error: error.toString(),
    });
  }
});

app.get('/tasks', async (req, res) => {
  try {
    let tasks = await knex.select('*').from('task');
    res.json(tasks);
  } catch (error) {
    res.status(500).json({
      message: 'failed to select tasks',
      error: error.toString(),
    });
  }
});

app.patch('/task/done', async (req, res) => {
  console.log(req.method, req.url, req.body);

  try {
    // let id = req.body.id;
    // let done = req.body.done;
    let { id, done } = req.body;
    await knex('task').update({ done }).where('id', id);
    res.json('ok');
  } catch (error) {
    res.status(500).json({
      message: 'failed to update task',
      error: error.toString(),
    });
  }
});

app.delete('/task', async (req, res) => {
  console.log(req.method, req.url, req.query);

  try {
    let images = await knex('task')
      .where({ id: req.query.id })
      .delete()
      .returning('image');
    console.log({ images });
    for (let image of images) {
      if (image) {
        let filename = path.join('uploads', image);
        fs.unlinkSync(filename);
      }
    }
    res.json('ok');
  } catch (error) {
    res.status(500).json({
      message: 'failed to delete task',
      error: error.toString(),
    });
  }
});

app.use(express.static('public'));
app.use(express.static('uploads'));

app.listen(8100, () => {
  console.log('listening on http://localhost:8100');
});
