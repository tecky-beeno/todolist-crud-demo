async function loadTasks() {
  let res = await fetch('/tasks');
  let tasks = await res.json();
  // let tasks = [
  //   { id: 1, content: 'buy milk', done: false },
  //   { id: 2, content: 'buy egg', done: false },
  // ];

  let html = '';

  // for (let i = 0; i < tasks.length; i++) {
  //   let task = tasks[i];
  // }
  for (let task of tasks) {
    // html = html + task.content
    // html += task.content;
    html += `
    <div class="task">
      <input
        type="checkbox"
        onchange="updateTaskDone(${task.id}, this.checked)"
        ${task.done ? 'checked' : ''}
      >
      ${task.content}
      <button onclick="deleteTask(${task.id})">X</button>
      ${task.image ? `<br><img src="${task.image}">` : ''}
    </div>`;
  }

  todolist.innerHTML = html;
}
loadTasks();

async function updateTaskDone(task_id, done) {
  console.log('updateTaskDone', task_id, done);
  fetch('/task/done', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      id: task_id,
      done: done,
    }),
  });
}

async function deleteTask(task_id) {
  console.log('deleteTask', task_id);
  await fetch('/task?id=' + task_id, {
    method: 'DELETE',
  });
  location.reload();
}
