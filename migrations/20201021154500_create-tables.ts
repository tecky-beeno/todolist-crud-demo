import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('task')) {
    return;
  }
  await knex.schema.createTable('task', (table) => {
    table.increments('id');
    table.text('content');
    table.boolean('done');
  });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('task')
}
