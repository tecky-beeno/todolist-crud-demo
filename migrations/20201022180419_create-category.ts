import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('category')){
        return
    }
    await knex.schema.createTable('category',table=>{
        table.increments('id')
        table.string('name')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('category')
}

